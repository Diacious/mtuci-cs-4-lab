# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence, res=[]):
    if not(len(res)):
        res = [y for y in sequence]
    elif len(res[0]) != len(sequence):
        res = [let + x for let in res for x in sequence if let.count(x) != sequence.count(x)]
    else:
        return res
    return get_permutations(sequence,res=res)








#if __name__ == '__main__':
#    #EXAMPLE
#    example_input = 'abc'
#    print('Input:', example_input)
#    print('Expected Output:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
#    print('Actual Output:', get_permutations(example_input))
    
#    # Put three example test cases here (for your sanity, limit your inputs
#    to be three characters or fewer as you will have n! permutations for a 
#    sequence of length n)


